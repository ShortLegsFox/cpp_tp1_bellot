#include "Cercle.h"
#include <cmath>
#include <iostream>

int Cercle::getDiametre() const {
    return Diametre;
}

void Cercle::setDiametre(int diametre) {
    Diametre = diametre;
}

Point Cercle::getCentre() const {
    return Centre;
}

void Cercle::setCentre(Point centre) {
    Centre = centre;
}

double Cercle::calculPerimetre() const {
    return 2 * M_PI * ((double)(this->Diametre)/2);
}

double Cercle::calculSurface() const {
    return M_PI * ((double)(this->Diametre)/2);
}

bool Cercle::estDansLeCercle(Point pointATester) const {
    double longueurX = pointATester.x - Centre.x;
    double longueurY = pointATester.y - Centre.y;
    double distanceATester = sqrt(pow(longueurX, 2) + pow(longueurY, 2));
    double rayonCercle = (double)Diametre/2;

    if (std::abs(distanceATester) <= rayonCercle)
        return true;
    else
        return false;
}

bool Cercle::estSurLeCercle(Point pointATester) const {
    double longueurX = pointATester.x - Centre.x;
    double longueurY = pointATester.y - Centre.y;
    double distanceATester = sqrt(pow(longueurX, 2) + pow(longueurY, 2));
    double rayonCercle = (double)Diametre/2;

    if (std::abs(distanceATester) == rayonCercle)
        return true;
    else
        return false;
}

void Cercle::Afficher() {
    std::cout << "-- Cercle --" << std::endl;
    std::cout << "Centre: " << this->Centre.x << "," << this->Centre.y << std::endl;
    std::cout << "Diametre: " << this->Diametre << std::endl;
    std::cout << "Perimetre: " << this->calculPerimetre() << std::endl;
    std::cout << "Surface: " << this->calculSurface() << std::endl;
}