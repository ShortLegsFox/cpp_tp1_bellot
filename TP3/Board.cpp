#include "Board.h"

Board::Board() : BoardLenght(0), BoardHeight(0), FreeSpaces(0) { }

Board::Board(int const boardLenght, int const boardHeight) : BoardLenght(boardLenght), BoardHeight(boardHeight) {
    FreeSpaces = boardLenght * boardHeight;
    BoardGrid = std::vector<std::vector<char>>(boardHeight, std::vector<char>(boardLenght, ' '));
}

void Board::PrintBoard() {
    for (int i = 0; i < BoardHeight; i++) {
        for (int j = 0; j < BoardLenght; j++) {
            std::cout << "+---";
        }
        std::cout << "+" << std::endl;

        for (int j = 0; j < BoardLenght; j++) {
            std::cout << "| " << BoardGrid[i][j] << " ";
        }
        std::cout << "|" << std::endl;

        if (i == BoardHeight - 1) {
            for (int j = 0; j < BoardLenght; j++) {
                std::cout << "+---";
            }
            std::cout << "+" << std::endl;
        }
    }
}

void Board::ClearBoard() {
    int counter = 1;
    for (int i = 0; i < BoardLenght; i++) {
        for (int j = 0; j < BoardHeight; j++) {
            BoardGrid[i][j] = static_cast<char>(counter + '0');
            counter++;
        }
    }
}

bool Board::IsThereAlignment(int numberInARow, char playerSymbol) {
    bool row = IsThereRow(numberInARow, playerSymbol);
    bool col = IsThereCol(numberInARow, playerSymbol);
    bool diagDesc = IsThereDiagonalDesc(numberInARow, playerSymbol);
    bool diagAsc = IsThereDiagonalAsc(numberInARow, playerSymbol);
    return (row || col || diagAsc || diagDesc);
}

bool Board::IsThereRow(int numberInRow, char symbol) {
    for (int i = 0; i < BoardHeight; i++){
        int counterRow = 0;
        for (int j = 0; j < BoardLenght; j++){
            if (BoardGrid[i][j] == symbol)
                counterRow += 1;
            if (counterRow == numberInRow)
                return true;
        }
    }
    return false;
}

bool Board::IsThereCol(int numberInRow, char symbol) {
    for (int i = 0; i < BoardLenght; i++){
        int counterCol = 0;
        for (int j = 0; j < BoardHeight; j++){
            if (BoardGrid[j][i] == symbol)
                counterCol += 1;
            if (counterCol == numberInRow)
                return true;
        }
    }
    return false;
}

bool Board::IsThereDiagonalAsc(int numberInRow, char symbol) {
    for (int i = BoardLenght - numberInRow; i >= 0; i--) {
        for (int j = 0; j <= BoardHeight - numberInRow; j++) {
            int countDiag = 0;
            for (int k = 0; k < numberInRow; k++) {
                if (BoardGrid[j + k][i + k] == symbol) {
                    countDiag += 1;
                }
            }
            if (countDiag == numberInRow) {
                return true;
            }
        }
    }
    return false;
}

bool Board::IsThereDiagonalDesc(int numberInRow, char symbol) {
    for (int i = 0; i <= BoardLenght - numberInRow; i++) {
        for (int j = 0; j <= BoardHeight - numberInRow; j++) {
            int countDiag = 0;
            for (int k = 0; k < numberInRow; k++) {
                if (BoardGrid[j + k][i + k] == symbol) {
                    countDiag += 1;
                }
            }
            if (countDiag == numberInRow) {
                return true;
            }
        }
    }
    return false;
}

int Board::GetFreeSpaces() const {
    return FreeSpaces;
}

void Board::DecrFreeSpaces() {
    FreeSpaces -= 1;
}

std::vector<std::vector<char>> & Board::GetBoardGrid() {
    return BoardGrid;
}
