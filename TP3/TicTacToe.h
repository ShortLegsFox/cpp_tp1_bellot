#ifndef TP3_TICTACTOE_H
#define TP3_TICTACTOE_H


#include "Game.h"
#include "Board.h"


class TicTacToe : public Game {
public:
    TicTacToe(Player &playerOne, Player &playerTwo, Board &gameBoard);
    void InitGame() override;
    bool CheckWinner(Player &player) override;
    void Play(Board &board, Player &player) override;
    bool IsInputValid(int input) override;
    bool IsSpaceFree(int input, Board &board) override;
    void SetMove(int input, char piece, Board &board) override;
};

#endif
