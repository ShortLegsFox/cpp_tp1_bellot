#ifndef TP3_GAME_H
#define TP3_GAME_H

#include "Board.h"
#include "Player.h"
#include <iostream>
#include <vector>

class Game {
public:
    Game(Player &playerOne, Player &playerTwo, Board &gameBoard);
    virtual bool CheckWinner(Player &player);
    virtual void InitGame() ;
    virtual void Play(Board &board, Player &player);
    virtual bool IsInputValid(int input);
    virtual bool IsSpaceFree(int input, Board &board);
    virtual void SetMove(int input, char piece, Board &board);
protected:
    Board& GameBoard;
    Player& PlayerOne;
    Player& PlayerTwo;
};

#endif
