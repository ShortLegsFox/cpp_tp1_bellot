#include <iostream>
#include <array>

std::string calculScore(int echangesJoueur1, int echangesJoueur2, int avantageJoueur, bool &stopBoucle) {

    std::array<std::string, 4> scoresEchanges = {"0", "15", "30", "40"};

    if (echangesJoueur1 == 3 && echangesJoueur2 == 3 && avantageJoueur == 1) {
        return "40 a 40 - Avantage joueur 1";
    }

    if (echangesJoueur1 == 3 && echangesJoueur2 == 3 && avantageJoueur == 2) {
        return "40 a 40 - Avantage joueur 2";
    }

    if (echangesJoueur1 == 4 && echangesJoueur2 == 3) {
        return "40 a 40 - Joueur 1 en tete";
    }

    if (echangesJoueur1 == 3 && echangesJoueur2 == 4) {
        return "40 a 40 - Joueur 2 en tete";
    }

    if ((echangesJoueur1 == 5 && echangesJoueur2 == 3) || (echangesJoueur1 == 4 && echangesJoueur2 < 3)) {
        stopBoucle = true;
        return "Victoire joueur 1 !";
    }

    if ((echangesJoueur1 == 3 && echangesJoueur2 == 5) || (echangesJoueur1 < 3 && echangesJoueur2 == 4)) {
        stopBoucle = true;
        return "Victoire joueur 2 !";
    }

    return scoresEchanges[echangesJoueur1] + " a " + scoresEchanges[echangesJoueur2];
}

int main() {

    bool stopBoucle = false;
    int numEchange = 1;
    int numJoueur;

    int echangesJoueur1 = 0;
    int echangesJoueur2 = 0;
    int avantageJoueur;

    while (!stopBoucle) {
        std::cout<<"Echange numero "<<numEchange<<std::endl;
        std::cout<<"Saisir le joueur gagnant l'échange (1 ou 2) : "<<std::endl;
        std::cin>>numJoueur;

        if (numJoueur == 1) {
            echangesJoueur1 += 1;
            avantageJoueur = 1;
        }

        else {
            echangesJoueur2 += 1;
            avantageJoueur = 2;
        }

        if (echangesJoueur2 == 4 && echangesJoueur1 == 4) {
            echangesJoueur2 = 3;
            echangesJoueur1 = 3;
        }

        std::cout<<calculScore(echangesJoueur1, echangesJoueur2, avantageJoueur, stopBoucle)<<std::endl;
        numEchange += 1;
    }


    return 0;
}
