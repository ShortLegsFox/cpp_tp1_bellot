#include <iostream>
#include "Board.h"
#include "TicTacToe.h"
#include "HumanPlayer.h"
#include "ComputerPlayer.h"
#include "ConnectFour.h"

void PlayerVsPlayer(int userGameChoice) {
    std::string playerNameOne;
    std::string playerNameTwo;

    std::cout<<"Nom du joueur 1 (X) : "<<std::endl;
    std::cin>>playerNameOne;
    std::cout<<"Nom du joueur 2 (O) : "<<std::endl;
    std::cin>>playerNameTwo;

    HumanPlayer playerOne(playerNameOne, Player::Piece::X);
    HumanPlayer playerTwo(playerNameTwo, Player::Piece::O);

    if (userGameChoice == 1) {
        Board gameBoard(3,3);
        TicTacToe tictactoe(playerOne, playerTwo, gameBoard);
        tictactoe.InitGame();
    }
    else if (userGameChoice == 2) {
        Board gameBoard(7,4);
        ConnectFour connectfour(playerOne, playerTwo, gameBoard);
        connectfour.InitGame();
    }

}

void PlayerChoice(int userGameChoice) {
    bool menuLoop = true;
    int userChoice = 0;

    while (menuLoop) {
        std::cout<<"Choisissez : "<<std::endl;
        std::cout<<" - 1 : Joueur contre Joueur"<<std::endl;
        std::cout<<" - 2 : Joueur contre Ordinateur"<<std::endl;
        std::cout<<" - 3 : Quitter"<<std::endl;
        std::cout<<"Choisissez (1, 2 ou 3) :"<<std::endl;
        std::cin>>userChoice;

        switch(userChoice){
            case 1:
                std::cout << "Joueur contre Joueur !" << std::endl;
                PlayerVsPlayer(userGameChoice);
                break;
            case 2:
                std::cout << "Joueur contre Ordinateur!" << std::endl;
                std::cout << "Coming soon..." << std::endl;
                break;
            case 3:
                std::cout<<"Au revoir !"<<std::endl;
                menuLoop = false;
                break;
            default:
                std::cout<<"Choix invalide ! Choisir une valeur correspondante aux éléments du menu"<<std::endl;
        }
    }
}

int main() {
    bool menuLoop = true;
    int userChoice = 0;

    while (menuLoop) {
        std::cout<<" ------ Menu Principal ------"<<std::endl;
        std::cout<<" - 1 : Morpion"<<std::endl;
        std::cout<<" - 2 : Puissance 4"<<std::endl;
        std::cout<<" - 3 : Quitter"<<std::endl;
        std::cout<<"Choisissez (1, 2 ou 3) :"<<std::endl;
        std::cin>>userChoice;

        switch(userChoice){
            case 1:
                std::cout << "Vous avez choisis Morpion !" << std::endl;
                PlayerChoice(userChoice);
                break;
            case 2:
                std::cout << "Vous avez choisis Puissance 4 !" << std::endl;
                PlayerChoice(userChoice);
                break;
            case 3:
                std::cout<<"Au revoir !"<<std::endl;
                menuLoop = false;
                break;
            default:
                std::cout<<"Choix invalide ! Choisir une valeur correspondante aux éléments du menu"<<std::endl;
        }
    }
    return 0;
}

