#include <iostream>
#include "Rectangle.h"

int Rectangle::getLongueur() const {
    return Longueur;
}

void Rectangle::setLongueur(int longueur) {
    Longueur = longueur;
}

int Rectangle::getLargeur() const {
    return Largeur;
}

void Rectangle::setLargeur(int largeur) {
    Largeur = largeur;
}

Point Rectangle::getCoinSuperieurGauche() const {
    return CoinSuperieurGauche;
}

void Rectangle::setCoinSuperieurGauche(Point coinSuperieurGauche) {
    CoinSuperieurGauche = coinSuperieurGauche;
}

void Rectangle::Afficher() const {
    std::cout << "-- Rectangle --" << std::endl;
    std::cout << "Longueur : " << this->Longueur << std::endl;
    std::cout << "Largeur : " << this->Largeur << std::endl;
    std::cout << "Perimetre : " << this->calculPerimetre() << std::endl;
    std::cout << "Surface : " << this->calculSurface() << std::endl;
}

int Rectangle::calculPerimetre() const {
    return 2*Longueur + 2*Largeur;
}

int Rectangle::calculSurface() const {
    return Longueur * Largeur;
}

bool Rectangle::PlusGrandPerimetre(Rectangle rectangleAComparer) const {
    if (this->calculPerimetre() > rectangleAComparer.calculPerimetre())
        return true;
    else
        return false;
}

bool Rectangle::PlusGrandeSurface(Rectangle rectangleAComparer) const {
    if (this->calculSurface() > rectangleAComparer.calculSurface())
        return true;
    else
        return false;
}

