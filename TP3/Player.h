#ifndef TP3_PLAYER_H
#define TP3_PLAYER_H

#include <string>
#include "Board.h"

class Player {
public:
    enum Piece {
        X,
        O
    };

    Player(std::string playerName, Player::Piece piece);
    std::string GetName() const;
    char GetPiece() const;

protected:
    const Player::Piece PlayerPiece;
    const std::string PlayerName;
};

#endif
