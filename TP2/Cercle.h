#ifndef TP2_CERCLE_H
#define TP2_CERCLE_H


#include "Point.h"

class Cercle {
private:
    int Diametre;
    Point Centre;

public:
    int getDiametre() const;
    void setDiametre(int diametre);
    Point getCentre() const;
    void setCentre(Point centre);

    double calculPerimetre() const ;
    double calculSurface() const ;
    bool estDansLeCercle(Point pointATester) const ;
    bool estSurLeCercle(Point pointATester) const ;
    void Afficher();
};


#endif
