#include "Game.h"

Game::Game(Player& playerOne, Player& playerTwo, Board& gameBoard) : PlayerOne(playerOne), PlayerTwo(playerTwo), GameBoard(gameBoard) { }

bool Game::CheckWinner(Player &player) {
    return (GameBoard.IsThereAlignment(0, player.GetPiece()));
}

void Game::InitGame() { }

void Game::Play(Board &board, Player &player) { }

bool Game::IsInputValid(int input) {
    return false;
}

bool Game::IsSpaceFree(int input, Board &board) {
    return false;
}

void Game::SetMove(int input, char piece, Board &board) {

}
