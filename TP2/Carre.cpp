//
// Created by ibellot on 18/10/2023.
//

#include <iostream>
#include "Carre.h"

Point Carre::getCoinSuperieurGauche1() const {
    return coinSuperieurGauche;
}

void Carre::setCoinSuperieurGauche1(Point coinsuperieurgauche) {
    coinSuperieurGauche = coinsuperieurgauche;
}

int Carre::getLargeur() const {
    return Largeur;
}

void Carre::setLargeur(int largeur) {
    Largeur = largeur;
    Longueur = largeur;
}

int Carre::getLongueur() const {
    return Longueur;
}

void Carre::setLongueur(int longueur) {
    Longueur = longueur;
    Largeur = longueur;
}

int Carre::calculPerimetre() const {
    return 2*Longueur + 2*Largeur;
}

int Carre::calculSurface() const {
    return Longueur * Largeur;
}

bool Carre::PlusGrandPerimetre(Carre rectangleAComparer) const {
    if (this->calculPerimetre() > rectangleAComparer.calculPerimetre())
        return true;
    else
        return false;
}

bool Carre::PlusGrandeSurface(Carre rectangleAComparer) const {
    if (this->calculSurface() > rectangleAComparer.calculSurface())
        return true;
    else
        return false;
}


void Carre::Afficher() const {
    std::cout << "-- Carre --" << std::endl;
    std::cout << "Longueur : " << this->Longueur << std::endl;
    std::cout << "Largeur : " << this->Largeur << std::endl;
    std::cout << "Perimetre : " << this->calculPerimetre() << std::endl;
    std::cout << "Surface : " << this->calculSurface() << std::endl;
}
