#ifndef TP3_COMPUTERPLAYER_H
#define TP3_COMPUTERPLAYER_H


#include "Player.h"

class ComputerPlayer : public Player {
public:
    ComputerPlayer(std::string playerName, Player::Piece piece);
private:
};


#endif
