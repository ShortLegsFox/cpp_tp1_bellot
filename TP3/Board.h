#ifndef TP3_BOARD_H
#define TP3_BOARD_H

#include <iostream>
#include <vector>

class Board {
public:
    Board();
    Board(int boardLenght, int boardHeight);
    int GetFreeSpaces() const;
    void DecrFreeSpaces();
    std::vector<std::vector<char>> & GetBoardGrid();
    void PrintBoard();
    void ClearBoard();
    bool IsThereAlignment(int numberInARow, char playerSymbol);
    bool IsThereDiagonalAsc(int numberInRow, char symbol);
    bool IsThereDiagonalDesc(int numberInRow, char symbol);
    bool IsThereCol(int numberInRow, char symbol);
    bool IsThereRow(int numberInRow, char symbol);

private:
    const int BoardLenght;
    const int BoardHeight;
    std::vector<std::vector<char>> BoardGrid;
    int FreeSpaces;
};

#endif
