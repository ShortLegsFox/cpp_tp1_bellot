#include <iostream>
#include <vector>
#include <random>


int somme(int valeur1, int valeur2) {

    return valeur1 + valeur2;
}

void sommeRemplaceParPointeur(int valeur1, int valeur2, int* pointeur) {

    *pointeur = somme(valeur1, valeur2);
}

void sommeRemplaceParReference(int valeur1, int valeur2, int& reference) {

    reference = somme(valeur1, valeur2);
}

void afficheTableau(std::vector<int> tableau, int taille) {

    for (int compt1 = 0; compt1 < taille; compt1++) {
        std::cout<<tableau[compt1]<<std::endl;
    }
}

void triTableauCroissant(std::vector<int> &tableau, int taille) {

    for (int compt1 = 0; compt1 < taille; compt1++) {
        for (int compt2 = compt1 + 1; compt2 < taille; compt2++) {
            if(tableau[compt1] > tableau[compt2]) {
                int temp = tableau[compt1];
                tableau[compt1] = tableau[compt2];
                tableau[compt2] = temp;
            }
        }
    }
}

void triTableauDecroissant(std::vector<int> &tableau, int taille) {

    for (int compt1 = 0; compt1 < taille; compt1++) {
        for (int compt2 = compt1 + 1; compt2 < taille; compt2++) {
            if(tableau[compt1] < tableau[compt2]) {
                int temp = tableau[compt1];
                tableau[compt1] = tableau[compt2];
                tableau[compt2] = temp;
            }
        }
    }
}

int main() {

    int valeur1;
    int valeur2;
    int resultat = 0;
    int taille;
    int choix = 1;
    bool stopBoucle = false;

    std::cout << "Saisir la premiere valeur de la somme : " << std::endl;
    while (!(std::cin >> valeur1)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Saisie invalide. Veuillez saisir un entier : "<<std::endl;
    }

    std::cout << "Saisir la seconde valeur de la somme : " << std::endl;
    while (!(std::cin >> valeur2)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Saisie invalide. Veuillez saisir un entier : "<<std::endl;
    }

    std::cout << "La somme de " << valeur1 << " et " << valeur2 << " est : " << somme(valeur1, valeur2) << std::endl;

    sommeRemplaceParPointeur(valeur1, valeur2, &resultat);

    std::cout << "La somme de " << valeur1 << " et " << valeur2 << " avec un pointeur est : " << resultat << std::endl;

    sommeRemplaceParReference(valeur1, valeur2, resultat);

    std::cout << "La somme de " << valeur1 << " et " << valeur2 << " avec une reference est : " << resultat
              << std::endl;

    std::cout << "Saisir la taille du tableau : " << std::endl;
    while (!(std::cin >> taille) || taille <= 0) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Saisie invalide. Veuillez saisir un entier : "<<std::endl;
    }

    std::vector<int> tableau(taille);

    std::random_device generateur;
    std::uniform_int_distribution<int> distribution(0, 9999);

    for (int compt1 = 0; compt1 < taille; compt1++) {
        tableau[compt1] = distribution(generateur);
    }

    std::cout << "Valeurs du tableau : " << std::endl;
    afficheTableau(tableau, taille);

    while (!stopBoucle) {

        std::cout << "Choisir une option : " << std::endl;
        std::cout << " - 1 - Tri croissant " << std::endl;
        std::cout << " - 2 - Tri decroissant " << std::endl;
        std::cout << " - 3 - Quitter " << std::endl;
        std::cin >> choix;
        while (choix != 1 && choix != 2 && choix != 3) {
            std::cout << "Valeur incorrecte, saisir un choix valide : " << std::endl;
            std::cout << " - 1 - Tri croissant " << std::endl;
            std::cout << " - 2 - Tri decroissant " << std::endl;
            std::cout << " - 3 - Quitter " << std::endl;
            std::cin >> choix;
        }

        switch (choix) {
            case 1:
                triTableauCroissant(tableau, taille);
                std::cout << "Valeurs triees par ordre croissant : " << std::endl;
                afficheTableau(tableau, taille);
                break;
            case 2:
                triTableauDecroissant(tableau, taille);
                std::cout << "Valeurs triees par ordre decroissant : " << std::endl;
                afficheTableau(tableau, taille);
                break;
            case 3:
                stopBoucle = true;
                break;
            default:
                std::cout << "Erreur : choix du tri non reconnu ... " << std::endl;
                break;
        }

    }

    return 0;
}
