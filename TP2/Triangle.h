#ifndef TP2_TRIANGLE_H
#define TP2_TRIANGLE_H

#include "Point.h"

class Triangle {
private:
    Point pointA;
    Point pointB;
    Point pointC;
    Point sommetTriangle;
public:
    Point getPointA() const;

    void setPointA(Point point);

    Point getPointB() const;

    void setPointB(Point point);

    Point getPointC() const;

    void setPointC(Point point);

    double distanceEntre(Point premierPoint, Point secondPoint) const;

    double calculBase() const;

    double calculSurface() const;

    double calculHauteur() const;

    bool estIsocele() const;

    bool estRectangle() const;

    bool estEquilateral() const;

    void Afficher() const;
};

#endif