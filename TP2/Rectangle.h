#ifndef TP2_RECTANGLE_H
#define TP2_RECTANGLE_H

#include "Point.h"

class Rectangle {
private:
    int Longueur;
    int Largeur;
    Point CoinSuperieurGauche;

public:
    virtual int getLongueur() const;
    virtual void setLongueur(int longueur);

    virtual int getLargeur() const;

    virtual void setLargeur(int largeur);

    Point getCoinSuperieurGauche() const;
    void setCoinSuperieurGauche(Point coinSuperieurGauche);

    virtual void Afficher() const;
    int calculPerimetre() const;
    int calculSurface() const;

    bool PlusGrandPerimetre(Rectangle rectangleAComparer) const;
    bool PlusGrandeSurface(Rectangle rectangleAComparer) const;

};


#endif
