#include "Player.h"

Player::Player(std::string playerName, Player::Piece piece) : PlayerPiece(piece), PlayerName(playerName) { }

char Player::GetPiece() const {
    if (PlayerPiece == X)
        return 'X';
    else if (PlayerPiece == O)
        return 'O';
    else
        return '?';
}

std::string Player::GetName() const {
    return PlayerName;
}

