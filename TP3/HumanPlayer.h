#ifndef TP3_HUMANPLAYER_H
#define TP3_HUMANPLAYER_H


#include "Player.h"
#include "Board.h"


class HumanPlayer : public Player {
public:
    HumanPlayer(std::string playerName, Player::Piece piece);
private:
};



#endif
