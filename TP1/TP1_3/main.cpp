#include <iostream>
#include <random>

int main() {

    int nombreAleatoire;
    int saisie;
    int compt;

    std::string prenom;
    std::string nom;

    std::cout<<"Saisir votre nom et votre prenom : "<<std::endl;
    std::cin>>nom>>prenom;

    if (!prenom.empty() && !nom.empty()) {

        for (char & c : nom){
            c = static_cast<char>(std::toupper(c));
        }

        for (char & c : prenom) {
            c = static_cast<char>(std::tolower(c));
        }
        prenom[0] = static_cast<char>(std::toupper(prenom[0]));
    }

    std::cout<< "Bonjour "<<prenom<<" "<<nom<<std::endl;

    std::cout<<"Generation d'un nombre aleatoire..."<<std::endl;

    std::random_device generateur;
    std::uniform_int_distribution<int> distribution(0, 1000);

    nombreAleatoire = distribution(generateur);

    std::cout<<nombreAleatoire<<" Saisir un nombre : "<<std::endl;
    std::cin>>saisie;

    compt = 1;
    while (saisie != nombreAleatoire) {

        if (saisie > nombreAleatoire) {
            std::cout<<"Trop grand !"<<std::endl;
        }
        else if (saisie < nombreAleatoire) {
            std::cout<<"Trop petit !"<<std::endl;
        }
        std::cout<<nombreAleatoire<<" Saisir un nombre : "<<std::endl;
        std::cin>>saisie;
        compt++;
    }

    std::cout<<"Bravo ! (Nombre d'essais : "<<compt<<")"<<std::endl;


    return 0;
}