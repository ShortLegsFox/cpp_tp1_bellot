#include "ConnectFour.h"

ConnectFour::ConnectFour(Player &playerOne, Player &playerTwo, Board &gameBoard) : Game(playerOne, playerTwo, gameBoard) { }

void ConnectFour::InitGame() {
    bool gameRevenge = true;
    while (gameRevenge){
        bool gameLoop = true;
        while (gameLoop) {
            this->Play(GameBoard, PlayerOne);
            if (this->CheckWinner(PlayerOne)) {
                GameBoard.PrintBoard();
                std::cout << "Victoire pour le joueur " << PlayerOne.GetName() << std::endl;
                gameLoop = false;
                gameRevenge = false;
            } else if (GameBoard.GetFreeSpaces() == 0) {
                GameBoard.PrintBoard();
                std::cout << "Match nul !" << std::endl;
                gameLoop = false;
            } else {
                this->Play(GameBoard, PlayerTwo);
                if (this->CheckWinner(PlayerTwo)) {
                    GameBoard.PrintBoard();
                    std::cout << "Victoire pour le joueur " << PlayerTwo.GetName() << std::endl;
                    gameLoop = false;
                    gameRevenge = false;
                } else if (GameBoard.GetFreeSpaces() == 0) {
                    GameBoard.PrintBoard();
                    std::cout << "Match nul !" << std::endl;
                    gameLoop = false;
                }
            }
        }
        if(gameRevenge) {
            int userChoice;
            std::cout<<"Une revanche ?"<<std::endl;
            std::cout<<" - 1 : Oui"<<std::endl;
            std::cout<<" - 2 : Non"<<std::endl;
            std::cin>>userChoice;
            if (userChoice == 2) {
                gameRevenge = false;
            }
        }
    }
}

bool ConnectFour::CheckWinner(Player &player) {
    return (GameBoard.IsThereAlignment(4, player.GetPiece()));
}

void ConnectFour::Play(Board &board, Player &player) {
    bool playLoop = true;

    while(playLoop) {
        board.PrintBoard();
        int playerMove;
        std::cout<<player.GetName()<<" - Saisir le numéro de la colonne sur laquelle vous voulez placer le pion (1 à 7)"<<std::endl;
        std::cin>>playerMove;
        playerMove = playerMove - 1;
        if (this->IsInputValid(playerMove) && this->IsSpaceFree(playerMove, GameBoard)) {
            this->SetMove(playerMove, player.GetPiece(), GameBoard);
            board.DecrFreeSpaces();
            playLoop = false;
        }
        else {
            std::cout<<"Mauvaise saisie, rééssayer..."<<std::endl;
        }
    }
}

bool ConnectFour::IsInputValid(int input) {
    return (input <= 6 && input >= 0);
}

bool ConnectFour::IsSpaceFree(int input, Board &board) {
    for (int i = 0; i < 4; i++) {
        if(board.GetBoardGrid()[i][input] == ' ')
            return true;
    }
    return false;
}

void ConnectFour::SetMove(int input, char piece, Board &board) {
    int x;
    for (int i = 0; i < 4; i++){
        if (board.GetBoardGrid()[i][input] == ' ')
            x = i;
    }
    int y = input;
    board.GetBoardGrid()[x][y] = piece;
}

