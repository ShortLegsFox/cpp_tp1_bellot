#include <iostream>
#include "Rectangle.h"
#include "Cercle.h"
#include "Triangle.h"
#include "Carre.h"

int main() {

    std::cout<<"Premier rectangle : "<<std::endl;
    Rectangle rect;

    rect.setLargeur(10);
    rect.setLongueur(25);

    rect.Afficher();

    std::cout<<"Second rectangle : "<<std::endl;
    Rectangle rect2;

    rect2.setLargeur(15);
    rect2.setLongueur(20);

    rect2.Afficher();

    if (rect.PlusGrandPerimetre(rect2))
        std::cout<<"Le premier rectangle a un plus grand perimetre que le second"<<std::endl;
    else
        std::cout<<"Le premier rectangle a le meme ou un plus petit perimetre que le second"<<std::endl;

    if (rect.PlusGrandeSurface(rect2))
        std::cout<<"Le premier rectangle a une plus grande surface que le second"<<std::endl;
    else
        std::cout<<"Le premier rectangle a la meme ou une plus petite surface que le second"<<std::endl;

    std::cout<<std::endl;

    Cercle cercle;

    Point pointO;
    pointO.x = 3;
    pointO.y = 7;

    cercle.setCentre(pointO);
    cercle.setDiametre(15);

    cercle.Afficher();

    Point pointATester;
    pointATester.x = 6;
    pointATester.y = 10;

    if (cercle.estSurLeCercle(pointATester))
        std::cout<<"Le point ("<<pointATester.x<<","<<pointATester.y<<") est sur le cercle"<<std::endl;
    else
        std::cout<<"Le point ("<<pointATester.x<<","<<pointATester.y<<") n'est pas sur le cercle"<<std::endl;

    if (cercle.estDansLeCercle(pointATester))
        std::cout<<"Le point ("<<pointATester.x<<","<<pointATester.y<<") est dans le cercle"<<std::endl;
    else
        std::cout<<"Le point ("<<pointATester.x<<","<<pointATester.y<<") n'est pas dans le cercle"<<std::endl;

    Triangle triangle;

    Point pointA;
    pointA.x = 1;
    pointA.y = 0;

    Point pointB;
    pointB.x = 3;
    pointB.y = 2;

    Point pointC;
    pointC.x = 3;
    pointC.y = 0;

    triangle.setPointA(pointA);
    triangle.setPointB(pointB);
    triangle.setPointC(pointC);

    triangle.Afficher();

    std::cout<<"Premier carre : "<<std::endl;
    Carre carre;

    carre.setLongueur(6);

    carre.Afficher();

    std::cout<<"Second carre : "<<std::endl;
    Carre carre2;

    carre2.setLongueur(8);

    carre2.Afficher();

    if (carre.PlusGrandPerimetre(carre2))
        std::cout<<"Le premier rectangle a un plus grand perimetre que le second"<<std::endl;
    else
        std::cout<<"Le premier rectangle a le meme ou un plus petit perimetre que le second"<<std::endl;

    if (carre.PlusGrandeSurface(carre2))
        std::cout<<"Le premier rectangle a une plus grande surface que le second"<<std::endl;
    else
        std::cout<<"Le premier rectangle a la meme ou une plus petite surface que le second"<<std::endl;

    std::cout<<std::endl;

    return 0;
}
