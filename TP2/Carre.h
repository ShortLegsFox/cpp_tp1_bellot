#ifndef TP2_CARRE_H
#define TP2_CARRE_H

#include "Rectangle.h"

class Carre {
private:
    Point coinSuperieurGauche;
    int Largeur;
    int Longueur;

public:
    Point getCoinSuperieurGauche1() const;

    void setCoinSuperieurGauche1(Point coinSuperieurGauche);

    int getLargeur() const ;

    void setLargeur(int largeur) ;

    int getLongueur() const ;

    void setLongueur(int longueur) ;

    int calculPerimetre() const;

    int calculSurface() const;

    bool PlusGrandPerimetre(Carre rectangleAComparer) const;

    bool PlusGrandeSurface(Carre rectangleAComparer) const;

    void Afficher() const ;
};


#endif
