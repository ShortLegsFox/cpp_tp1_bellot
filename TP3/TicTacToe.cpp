#include "TicTacToe.h"

TicTacToe::TicTacToe(Player& playerOne, Player& playerTwo, Board& gameBoard) : Game(playerOne, playerTwo, gameBoard) { }

void TicTacToe::InitGame() {
    bool gameRevenge = true;
    while (gameRevenge){
        GameBoard.ClearBoard();
        bool gameLoop = true;
        while (gameLoop) {
            this->Play(GameBoard, PlayerOne);
            if (this->CheckWinner(PlayerOne)) {
                GameBoard.PrintBoard();
                std::cout << "Victoire pour le joueur " << PlayerOne.GetName() << std::endl;
                gameLoop = false;
                gameRevenge = false;
            } else if (GameBoard.GetFreeSpaces() == 0) {
                std::cout << "Match nul !" << std::endl;
                gameLoop = false;
            } else {
                this->Play(GameBoard, PlayerTwo);
                if (this->CheckWinner(PlayerTwo)) {
                    GameBoard.PrintBoard();
                    std::cout << "Victoire pour le joueur " << PlayerTwo.GetName() << std::endl;
                    gameLoop = false;
                    gameRevenge = false;
                } else if (GameBoard.GetFreeSpaces() == 0) {
                    GameBoard.PrintBoard();
                    std::cout << "Match nul !" << std::endl;
                    gameLoop = false;
                }
            }
        }
        if(gameRevenge) {
            int userChoice;
            std::cout<<"Une revanche ?"<<std::endl;
            std::cout<<" - 1 : Oui"<<std::endl;
            std::cout<<" - 2 : Non"<<std::endl;
            std::cin>>userChoice;
            if (userChoice == 2) {
                gameRevenge = false;
            }
        }
    }
}

bool TicTacToe::CheckWinner(Player &player) {
    return (GameBoard.IsThereAlignment(3, player.GetPiece()));
}

void TicTacToe::Play(Board &board, Player &player) {
    bool playLoop = true;

    while(playLoop) {
        board.PrintBoard();
        int playerMove;
        std::cout<<player.GetName()<<" - Saisir le numéro de la case sur laquelle vous voulez placer le pion (1 à 9)"<<std::endl;
        std::cin>>playerMove;
        if (this->IsInputValid(playerMove) && this->IsSpaceFree(playerMove, GameBoard)) {
            this->SetMove(playerMove, player.GetPiece(), GameBoard);
            board.DecrFreeSpaces();
            playLoop = false;
        }
        else {
            std::cout<<"Mauvaise saisie, rééssayer..."<<std::endl;
        }
    }
}

bool TicTacToe::IsInputValid(int input) {
    return (input <= 9 && input >=1);
}

bool TicTacToe::IsSpaceFree(int input, Board &board) {
    int x = (input - 1) / 3;
    int y = (input - 1) % 3;
    return (board.GetBoardGrid()[x][y] != 'X' || board.GetBoardGrid()[x][y] != 'O');
}

void TicTacToe::SetMove(int input, char piece, Board &board) {
    int x = (input - 1) / 3;
    int y = (input - 1) % 3;
    board.GetBoardGrid()[x][y] = piece;
}
