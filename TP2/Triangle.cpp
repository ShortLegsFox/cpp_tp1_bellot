#include "Triangle.h"
#include <cmath>
#include <iostream>


Point Triangle::getPointA() const {
    return pointA;
}

void Triangle::setPointA(Point point) {
    pointA = point;
}

Point Triangle::getPointB() const {
    return pointB;
}

void Triangle::setPointB(Point point) {
    Triangle::pointB = point;
}

Point Triangle::getPointC() const {
    return pointC;
}

void Triangle::setPointC(Point point) {
    Triangle::pointC = point;
}

double Triangle::distanceEntre(Point premierPoint, Point secondPoint) const {
    return sqrt(pow(premierPoint.x - secondPoint.x, 2) + pow(premierPoint.y - secondPoint.y, 2));
}

double Triangle::calculBase() const {
    double distAB = distanceEntre(pointA, pointB);
    double distBC = distanceEntre(pointB, pointC);
    double distCA = distanceEntre(pointC, pointA);

    return std::max(distAB, std::max(distBC, distCA));
}

double Triangle::calculSurface() const {
    double distAB = distanceEntre(pointA, pointB);
    double distBC = distanceEntre(pointB, pointC);
    double distCA = distanceEntre(pointC, pointA);

    double demiPerimetre = (distAB + distBC + distCA)/2;

    return sqrt(demiPerimetre * (demiPerimetre - distAB) * (demiPerimetre - distBC) * (demiPerimetre - distCA));
}

double Triangle::calculHauteur() const {
    return (2 * calculSurface()) / calculBase();
}

bool Triangle::estIsocele() const {
    double distAB = distanceEntre(pointA, pointB);
    double distBC = distanceEntre(pointB, pointC);
    double distCA = distanceEntre(pointC, pointA);

    return (distAB == distBC || distAB == distCA || distBC == distCA);
}

bool Triangle::estRectangle()  const {
    double distAB = distanceEntre(pointA, pointB);
    double distBC = distanceEntre(pointB, pointC);
    double distCA = distanceEntre(pointC, pointA);

    double hypotenuse = std::max(distAB, std::max(distBC, distCA));
    if (hypotenuse == sqrt(pow(distAB, 2) + pow(distBC, 2)))
        return true;
    else if (hypotenuse == sqrt(pow(distAB,2) + pow(distCA,2)))
        return true;
    else if (hypotenuse == sqrt(pow(distBC,2) + pow(distCA,2)))
        return true;
    else
        return false;
}

bool Triangle::estEquilateral() const {
    double distAB = distanceEntre(pointA, pointB);
    double distBC = distanceEntre(pointB, pointC);
    double distCA = distanceEntre(pointC, pointA);

    return (distAB == distBC && distAB == distCA);
}

void Triangle::Afficher() const {
    std::cout << "Triangle : " << std::endl;
    std::cout << "Longueur AB: " << this->distanceEntre(pointA, pointB) << std::endl;
    std::cout << "Longueur BC: " << this->distanceEntre(pointB, pointC) << std::endl;
    std::cout << "Longueur CA: " << this->distanceEntre(pointC, pointA) << std::endl;
    std::cout << "Base : " << this->calculBase() << std::endl;
    std::cout << "Longueur : " << this->calculHauteur() << std::endl;
    std::cout << "Surface : " << this->calculSurface() << std::endl;
    if (this->estIsocele())
        std::cout << "Le triangle est isocele" <<std::endl;
    if (this->estRectangle())
        std::cout << "Le triangle est rectangle"<<std::endl;
    if (this->estEquilateral())
        std::cout << "le triangle est equilateral" <<std::endl;
}
